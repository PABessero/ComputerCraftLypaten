-- BaseControl
-- (c) 2022 PABessero
-- Version 1.0

local common = {}

function common.formatNumber(number)
    local rankNames = {'', 'k', 'M', 'B', 'T', 'P', 'E', 'Z', 'Y'}
    local rank = math.floor(math.log10(number) / 3 )
    local trimmed = number / (10 ^ (rank * 3))

    if number == 0 then
        return '0'
    end

    return string.format("%.3f%s", trimmed, rankNames[rank + 1])
end

return common
