-- BaseControl
-- (c) 2022 PABessero
-- Version 1.0

local conf = require('config')
local common = require('common.functions')

local MekanismTank = {}

function MekanismTank.getContentName(tank)
    return tank.getStored().name
end

function MekanismTank.getContentAmount(tank)
    return tostring(tank.getStored().amount)
end

function MekanismTank.getMaxCapacity(tank)
    return tostring(tank.getTankCapacity())
end

function MekanismTank.getPercentage(tank)
    return tonumber(string.format("%.3f", tank.getFilledPercentage()*100))
end


function MekanismTank.sendData(tank)
    conf.ws.send(tank)
end


function MekanismTank.setupAllTanks()
    for i, peripheralId in pairs(peripheral.getNames()) do
        if string.find(peripheralId, 'dynamicValve') then
            local toInsert = true
            if conf.tanks ~= {} then
                for i2, v2 in pairs(conf.tanks) do
                    if string.find(peripheralId, v2.id) then
                        v2.wrapper = peripheral.wrap(peripheralId)
                        toInsert = false
                        break
                    end
                end
            end
            if toInsert then
                table.insert(conf.tanks, { name = peripheralId,
                                           id = peripheralId,
                                           wrapper = peripheral.wrap(peripheralId)})
            end
        end
    end
end


function MekanismTank.mainLoop()
    for i, v in pairs(conf.tanks) do
        tank = v.wrapper
        v.content = MekanismTank.getContentName(tank)
        v.maxCapacity = MekanismTank.getMaxCapacity(tank)
        v.amount = MekanismTank.getContentAmount(tank)

        if conf.websocketServer ~= '' then
            MekanismTank.sendData(v)
        end
    end
end

return MekanismTank
