-- BaseControl
-- (c) 2022 PABessero
-- Version 1.0

local conf = require('config')
local common = require('common.functions')

---A test module
---@module MekanismInductionMatrix
local MekanismInductionMatrix = {}

---Gets current energy for peripheral
---@param matrix table Induction Matrix peripheral
---@return number Current energy amount (in RF)
function MekanismInductionMatrix.getEnergy(matrix)
    return tostring(matrix.getEnergy()/2.5)
end

---Gets max energy for peripheral
---@param matrix table Induction Matrix peripheral
---@return number Maximum energy amount (in RF)
function MekanismInductionMatrix.getMaxEnergy(matrix)
    return tostring(matrix.getMaxEnergy()/2.5)
end

---Gets filled energy percentage for peripheral, rounded to the nearest value
---@param matrix table Induction Matrix peripheral
---@return number Filled percentage (rounded to the unit)
function MekanismInductionMatrix.getPercentage(matrix)
    return tostring(matrix.getEnergyFilledPercentage())
end

---Gets filled energy percentage for peripheral
---@param matrix table Induction Matrix peripheral
---@return number Filled percentage (precise)
function MekanismInductionMatrix.getPercentagePrecise(matrix)
    return tostring(tonumber(MekanismInductionMatrix.getEnergy(matrix)) / tonumber(MekanismInductionMatrix.getMaxEnergy(matrix)) * 100)
end

---Gets last energy input for peripheral
---@param matrix table Induction Matrix peripheral
---@return number Last input (in RF/t)
function MekanismInductionMatrix.getLastInput(matrix)
    return tostring(matrix.getLastInput() / 2.5)
end

---Gets last energy output for peripheral
---@param matrix table Induction Matrix peripheral
---@return number Last output (in RF/t)
function MekanismInductionMatrix.getLastOutput(matrix)
    return tostring(matrix.getLastOutput() / 2.5)
end

function MekanismInductionMatrix.updateScreen(matrix)
    local energy = common.formatNumber(matrix.energy) .. 'FE'
    local maxEnergy = common.formatNumber(matrix.maxEnergy) .. 'FE'
    local energyPercentage = matrix.percentage*100 .. '%'
    local powerStorageText = energy .. ' / ' .. maxEnergy .. ' (' .. energyPercentage .. ')'
    local input = '+ ' .. common.formatNumber(matrix.lastInput) .. 'FE/t'
    local output = '- ' .. common.formatNumber(matrix.lastOutput) .. 'FE/t'
    print(input)
    print(matrix.lastInput)
    local width, height = matrix.powerStorageWindow.getSize()
    matrix.powerStorageWindow.clear()
    matrix.powerStorageWindow.setCursorPos(width - #powerStorageText, 1)
    matrix.powerStorageWindow.write(powerStorageText)
    if conf.enabled.goggles then
        local defaultColor = 0x03fcfc
        conf.goggles.drawStringWithId(matrix.goggles.main.stringName, matrix.name ,matrix.goggles.main.posX, matrix.goggles.main.posY, matrix.goggles.main.color or defaultColor)
        conf.goggles.drawStringWithId(matrix.goggles.storage.stringName, powerStorageText, matrix.goggles.storage.posX, matrix.goggles.storage.posY, matrix.goggles.storage.color or defaultColor)
        conf.goggles.drawStringWithId(matrix.goggles.lastIn.stringName, input, matrix.goggles.lastIn.posX, matrix.goggles.lastIn.posY, matrix.goggles.lastIn.color or defaultColor)
        conf.goggles.drawStringWithId(matrix.goggles.lastOut.stringName, output, matrix.goggles.lastOut.posX, matrix.goggles.lastOut.posY, matrix.goggles.lastOut.color or defaultColor)

    end
end

---Sends induction matrix data to the websocket server
---@param matrix table full induction matrix TABLE
function MekanismInductionMatrix.sendData(matrix)
    conf.ws.send(matrix)
end

---setupAllMatrix
function MekanismInductionMatrix.setupAllMatrix()
    for i, peripheralId in pairs(peripheral.getNames()) do
        if string.find(peripheralId, 'inductionPort') then
            local toInsert = true
            if conf.inductionMatrix ~= {} then
                for i2, v2 in pairs(conf.inductionMatrix) do
                    if string.find(peripheralId, v2.id) then
                        v2.wrapper = peripheral.wrap(peripheralId)
                        toInsert = false
                        break
                    end
                end
            end
            if toInsert then
                table.insert(conf.inductionMatrix, { name = peripheralId, id = peripheralId, wrapper = peripheral.wrap(peripheralId)})
            end
        end
    end
end

---setupAllScreens
---@param parent table
function MekanismInductionMatrix.setupAllScreens(parent)
    local line = 3

    for i, matrix in pairs(conf.inductionMatrix) do
        if matrix.monitor then
            matrix.monitor.wrapper = peripheral.wrap(matrix.monitor.id)
        end

        parentWidth, parentHeight = parent.getSize()


        matrix.nameWindow = window.create(parent, 1, line, parentWidth, 1)
        matrix.nameWindow.setBackgroundColor(colors.blue)
        matrix.nameWindow.clear()
        matrix.nameWindow.setCursorPos(2 , 1)
        matrix.nameWindow.write(matrix.name)

        line = line + 1

        matrix.powerStorageWindow = window.create(parent, 1, line, parentWidth, 1)
        matrix.powerStorageWindow.setBackgroundColor(colors.lightBlue)
        matrix.powerStorageWindow.clear()
        matrix.powerStorageWindow.setCursorPos(2 , 1)
        matrix.powerStorageWindow.write('powerStorageWindow')

        line = line+2
    end
end

function MekanismInductionMatrix.setupGoggles()
    local line = 10
    for i, matrix in pairs(conf.inductionMatrix) do
        matrix.goggles = {}
        matrix.goggles.main = {}
        matrix.goggles.main.stringName = matrix.name .. '_' .. 'name'
        matrix.goggles.main.posX = 10
        matrix.goggles.main.posY = line

        line = line + 10

        matrix.goggles.storage = {}
        matrix.goggles.storage.stringName = matrix.name .. '_' .. 'energyStorage'
        matrix.goggles.storage.posX = 15
        matrix.goggles.storage.posY = line

        line = line + 10

        matrix.goggles.lastIn = {}
        matrix.goggles.lastIn.stringName = matrix.name .. '_' .. 'lastIn'
        matrix.goggles.lastIn.posX = 25
        matrix.goggles.lastIn.posY = line
        matrix.goggles.lastIn.color = 0x34eb37

        line = line + 10

        matrix.goggles.lastOut = {}
        matrix.goggles.lastOut.stringName = matrix.name .. '_' .. 'lastOut'
        matrix.goggles.lastOut.posX = 25
        matrix.goggles.lastOut.posY = line
        matrix.goggles.lastOut.color = 0xeb3434

        line = line + 10
    end
end

---mainLoop
function MekanismInductionMatrix.mainLoop()
    for i, v in pairs(conf.inductionMatrix) do
        matrix = v.wrapper
        v.energy = MekanismInductionMatrix.getEnergy(matrix)
        v.maxEnergy = MekanismInductionMatrix.getMaxEnergy(matrix)
        v.percentage = MekanismInductionMatrix.getPercentage(matrix)
        v.percentagePrecise = MekanismInductionMatrix.getPercentagePrecise(matrix)
        v.lastInput = MekanismInductionMatrix.getLastInput(matrix)
        v.lastOutput = MekanismInductionMatrix.getLastOutput(matrix)

        sentData = {}
        sentData.id = v.id
        sentData.name = v.name
        sentData.energy = v.energy
        sentData.maxEnergy = v.maxEnergy
        sentData.lastInput = v.lastInput
        sentData.lastOutput = v.lastOutput

        if conf.websocketServer ~= '' then
            MekanismInductionMatrix.sendData(sentData)
        end
        if conf.enabled.goggles then

        end
        MekanismInductionMatrix.updateScreen(v)
    end
end

return MekanismInductionMatrix
