local password = "computercraft"
print("Enter Password:")
local input = read("*")

if input == password then
    print("Correct password. Access Granted")
else
    print("Incorrect password. Access Denied")
end

function set_enabled()
    local mekanism = settings.get('enabled.mekanism', "Y")
    repeat
        if mekanism ~= "Y" or mekanism ~= "N" then
            print("Invalid Input")
        end
        print("Enable Mekanism (Y/N) default Y")
        mekanism = string.upper(input())
    until mekanism == 'Y' or mekanism == 'N'
    if mekanism == 'Y' then
        settings.set('enabled.mekanism', true)
    elseif mekanism == 'N' then
        settings.set('enabled.mekanism', false)
    end

    local refined_storage = settings.get('enabled.refined_storage', "Y")
    repeat
        if refined_storage ~= "Y" or refined_storage ~= "N" then
            print("Invalid Input")
        end
        print("Enable Refined Storage (Y/N) default Y")
        refined_storage = string.upper(input())
    until refined_storage == 'Y' or refined_storage == 'N'
    if refined_storage == 'Y' then
        settings.set('enabled.refined_storage', true)
    elseif refined_storage == 'N' then
        settings.set('enabled.refined_storage', false)
    end

    local create = settings.get('enabled.create', "Y")
    repeat
        if create ~= "Y" or create ~= "N" then
            print("Invalid Input")
        end
        print("Enabled Create (Y/N) default Y")
        create = string.upper(input())
    until create == 'Y' or create == 'N'
    if create == 'Y' then
        settings.set('enabled.create', true)
    elseif create == 'N' then
        settings.set('enabled.create', false)
    end

    local goggles = settings.get('enabled.goggles', "Y")
    repeat
        if goggles ~= "Y" then
            print("Invalid Input")
        end
        print("Enabled Goggles (Y/N_ default Y")
        goggles = string.upper(input())
    until goggles == 'Y' or goggles == 'N'
    if goggles == 'Y' then
        settings.set('enabled.goggles', true)
    elseif goggles == 'N' then
        settings.set('enabled.goggles', false)
    end

    settings.set('configured', true)
    settings.save('.settings')
end

function prepare_goggles()  end
