monitor = peripheral.wrap('monitor_0')
train_station = peripheral.wrap('top')
crafts = peripheral.wrap('left')
rs_bridge = peripheral.wrap('back')
width, height = monitor.getSize()

monitor.clear()

station = window.create(monitor, 1, 1, width, 1)
station.setBackgroundColor(colors.blue)

tracks = window.create(monitor, 1, 2, width, 1)
tracks.setBackgroundColor(colors.gray)

processors = window.create(monitor, 1, 3, width, 1)
processors.setBackgroundColor(colors.gray)

sturdy_sheets = window.create(monitor, 1, 4, width, 1)
sturdy_sheets.setBackgroundColor(colors.gray)

rs_crafts = window.create(monitor, 1, 7 , width, 1)
rs_crafts.setBackgroundColor(colors.red)

rs_crafts_body = window.create(monitor, 1, 8, width, 15)
rs_crafts_body.setBackgroundColor(colors.orange)


while true do
    station.clear()
    station.setCursorPos(1,1)
    station.write(train_station.getLine(1))

    tracks.clear()
    tracks.setCursorPos(1,1)
    tracks.write(crafts.getLine(1))

    processors.clear()
    processors.setCursorPos(1, 1)
    processors.write(crafts.getLine(2))

    sturdy_sheets.setCursorPos(1,1)
    sturdy_sheets.clear()
    sturdy_sheets.write(crafts.getLine(3))

    rs_crafts.clear()
    rs_crafts.setCursorPos(1,1)
    rs_crafts.write('Currently Crafting:')

    rs_crafts_body.clear()
    rs_crafts_body.setCursorPos(5, 1)
    items = rs_bridge.listCraftableItems()
    v = 1
    for i, item in pairs(items) do
        if rs_bridge.isItemCrafting(item.name) then
            rs_crafts_body.setCursorPos(5, v)
            rs_crafts_body.write(item.displayName)
            v = v + 1
        end
    end

    sleep(1)
end
