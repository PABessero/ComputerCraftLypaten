-- BaseControl
-- (c) 2022 PABessero
-- Version 1.0

inductionMatrix = require('mekanism.MekanismInductionMatrix')
mekanismTanks = require('mekanism.MekanismTank')
conf = require('config')

settings.load('.settings')

configured = settings.get('configured', false)
if not configured then
    error("The configurator hasn't been run, please use setup first")
end

conf.ws = http.websocket(conf.websocketServer)


local mainScreen = peripheral.wrap(conf.mainMonitor.id)
conf.mainMonitor.width, conf.mainMonitor.height = mainScreen.getSize()

local menuWindow = window.create(mainScreen, 1, 1, conf.mainMonitor.width - conf.menuWindow.width, conf.mainMonitor.height)
menuWindow.clear()
menuWindow.setBackgroundColor(colors.gray)

local mekanismPowerWindow = window.create(mainScreen, conf.menuWindow.width + 2, 1, conf.mainMonitor.width - conf.menuWindow.width, conf.mainMonitor.height)
mekanismPowerWindow.clear()
mekanismPowerWindow.setBackgroundColor(colors.lightGray)

local oldTerm = term.redirect(mainScreen)
    paintutils.drawLine(conf.menuWindow.width+1, 1, conf.menuWindow.width+1, conf.mainMonitor.height)
term.redirect(oldTerm)

if conf.enabled.mekanism then
    inductionMatrix.setupAllMatrix()
    inductionMatrix.setupAllScreens(mekanismPowerWindow)
    inductionMatrix.setupGoggles()
    mekanismTanks.setupAllTanks()
end

if conf.enabled.goggles then
    conf.goggles = peripheral.find('arController')
    conf.goggles.clear()
end

if conf.enabled.create then

end

while true do
    if conf.enabled.mekanism then
        inductionMatrix.mainLoop()
        mekanismTanks.mainLoop()
    end

    sleep(0.5)
end
