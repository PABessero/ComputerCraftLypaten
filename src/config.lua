-- BaseControl
-- (c) 2022 PABessero
-- Version 1.0

local config = {}

config.websocketServer = ''

config.inductionMatrix = {
    {
        name = 'Moria Power Storage',
        id = 'inductionPort_4'
    }
}

config.tanks = {

}

config.mainMonitor = {
    name = 'Main',
    id = 'monitor_0'
}

config.menuWindow = {
    width = 13
}

config.enabled = {
    mekanism = true,
    goggles = false,
    refined_storage = false,
    create = false
}

return config
